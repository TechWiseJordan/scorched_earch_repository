﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
//using C3.XNA;

namespace JSTankGame
{
    //Background(this, spriteBatch, backgroundTex)
    class Background : DrawableGameComponent
    {
        SpriteBatch spriteBatch;
        Texture2D backgroundTex;

        List<Rectangle> rigidBodyList;
        public List<Rectangle> RigidBodyList { get => rigidBodyList; }
        public Background(Game game, SpriteBatch spriteBatch, Texture2D backgroundTex) : base(game)
        {
            this.spriteBatch = spriteBatch;
            this.backgroundTex = backgroundTex;

            rigidBodyList = new List<Rectangle>();
            rigidBodyList.Add(new Rectangle(0, 714, 1280, 750));  //Floor
            rigidBodyList.Add(new Rectangle(0, 0, 1280, 50));  //Roof
            rigidBodyList.Add(new Rectangle(0, 0, 5, 720));  //Left Wall
            rigidBodyList.Add(new Rectangle(1276, 0, 1281, 720));  //Right Wall
            //rigidBodyList.Add(new Rectangle(0, 500, 1280, 500));  //Testing floor
        }

        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
            spriteBatch.Draw(backgroundTex, new Rectangle(0, 0, 1280, 720), Color.White); // 1280x720 or 1920x1080

            //foreach (Rectangle r in rigidBodyList)
                //spriteBatch.DrawRectangle(r, Color.Red);
            spriteBatch.End();
            base.Draw(gameTime);
        }

        public override void Update(GameTime gameTime)
        {

            base.Update(gameTime);
        }
    }
}

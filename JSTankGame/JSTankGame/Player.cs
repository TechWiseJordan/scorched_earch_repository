﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
//using C3.XNA;
using PROG2370CollisionLibrary;

namespace JSTankGame
{
    class Player : DrawableGameComponent
    {
        SpriteBatch spriteBatch;
        Texture2D playerTexture;
        Texture2D bulletTexture;
        Background background;

        const int FRAMEWIDTH = 20;     //all values from spritesheet.txt
        const int FRAMEHEIGHT = 13;
        const float SCALE = 1.8f;

        const int STANDFRAME = 0;
        const int FIRSTWALKFRAME = 1;
        const int WALKFRAMES = 11;
        const int JUMPFRAME = 12;

        static int turn = 0;

        private int currentFrame = STANDFRAME;      //our initial frame (when game starts)
        List<Rectangle> playerFrames;
        SpriteEffects spriteDirection;  //determines what direction the texture is pointing

        const int FRAMEDELAYMAXCOUNT = 3;  //key change every 3 frames
        int currentFrameDelayCount = 0;          // increments every frame, resets every FRAMEDELAYMAXCOUNT

        const float GRAVITY = 0.02f;        //some constant that adds a force downward.

        int spawnLocationX = 200;
        int spawnLocationY = 200;
        bool isGrounded = false;
        Point pos;
        int turretAngle = 45;
        double turretAngleRad = 0;
        int power = 500;
        int currentPower = 0;
        bool isBot;
        bool bulletFlying = false;
        const int JUMPPOWER = -11;   //negative because of inverted co-oridinates (change to suit your max jump height)
        const float JUMPSTEP = 1.3f;  // the amount of movement per frame (gravity reduces this)

        const float SPEED = 1.5f;
        Vector2 velocity;
        Vector2 bulletVelocity;

        Rectangle player;
        Rectangle bullet;

        public Player(Game game, SpriteBatch spriteBatch, Texture2D playerTexture, Texture2D bulletTexture, int spawnLocationX, int spawnLocationY, bool isBot, Background background) : base(game)
        {
            this.spriteBatch = spriteBatch;
            this.playerTexture = playerTexture;
            this.background = background;
            this.bulletTexture = bulletTexture;
            this.spawnLocationX = spawnLocationX;
            this.spawnLocationY = spawnLocationY;
            this.isBot = isBot;

            player = new Rectangle(spawnLocationX,spawnLocationY, (int)(SCALE * FRAMEWIDTH), (int)(SCALE * FRAMEHEIGHT));
            bullet = new Rectangle(0, 0, (int)(SCALE * 2), (int)(SCALE * 2));

            velocity = new Vector2(0);
            bulletVelocity = new Vector2(0);

            playerFrames = new List<Rectangle>();

            //add the default frame
            playerFrames.Add(new Rectangle(0, 0, FRAMEWIDTH, FRAMEHEIGHT));

            //the walk frames
            playerFrames.Add(new Rectangle(0, 0, FRAMEWIDTH, FRAMEHEIGHT));
            playerFrames.Add(new Rectangle(20, 0, FRAMEWIDTH, FRAMEHEIGHT));
            playerFrames.Add(new Rectangle(40, 0, FRAMEWIDTH, FRAMEHEIGHT));
            playerFrames.Add(new Rectangle(60, 0, FRAMEWIDTH, FRAMEHEIGHT));
            playerFrames.Add(new Rectangle(80, 0, FRAMEWIDTH, FRAMEHEIGHT));
            playerFrames.Add(new Rectangle(100, 0, FRAMEWIDTH, FRAMEHEIGHT));
            playerFrames.Add(new Rectangle(120, 0, FRAMEWIDTH, FRAMEHEIGHT));
            playerFrames.Add(new Rectangle(140, 0, FRAMEWIDTH, FRAMEHEIGHT));
            playerFrames.Add(new Rectangle(160, 0, FRAMEWIDTH, FRAMEHEIGHT));
            playerFrames.Add(new Rectangle(180, 0, FRAMEWIDTH, FRAMEHEIGHT));
            playerFrames.Add(new Rectangle(200, 0, FRAMEWIDTH, FRAMEHEIGHT));
            playerFrames.Add(new Rectangle(220, 0, FRAMEWIDTH, FRAMEHEIGHT));     

            spriteDirection = SpriteEffects.None;
        }

        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
            spriteBatch.Draw(playerTexture,
                player,                                             // containing rectangle
                playerFrames.ElementAt<Rectangle>(currentFrame),     // our current key frame
                Color.White,
                0f,             //rotation
                new Vector2(0),    //origin 
                spriteDirection, // for animation only: the direction to face the texture
                0f      //layerdept
                );
            spriteBatch.Draw(bulletTexture, bullet, Color.Pink);
            //spriteBatch.DrawRectangle(player, Color.Yellow);
            spriteBatch.End();
            base.Draw(gameTime);
        }

        public override void Update(GameTime gameTime)
        {
            float deltaTime = (float)gameTime.ElapsedGameTime.TotalMilliseconds;

            velocity.X = 0;
            velocity.Y += GRAVITY * deltaTime;

            bulletVelocity.Y += GRAVITY * deltaTime;

            //get input
            if (!isBot && turn == 0)
            {
                KeyboardState keyState = Keyboard.GetState();

                if (keyState.IsKeyDown(Keys.Right))
                {
                    turretAngle--;
                }
                if (keyState.IsKeyDown(Keys.Left))
                {
                    turretAngle++;
                }
                if (keyState.IsKeyDown(Keys.Up))
                {
                    if (power < 1000)
                    {
                        power++;
                    }
                }
                if (keyState.IsKeyDown(Keys.Down))
                {
                    if (power > 0)
                    {
                        power--;
                    }
                }
                if (keyState.IsKeyDown(Keys.D))
                {
                    velocity.X = SPEED;
                }
                if (keyState.IsKeyDown(Keys.A))
                {
                    velocity.X = -SPEED;
                }
                if (keyState.IsKeyDown(Keys.Space))
                {
                    if (!bulletFlying && isGrounded)
                    {
                        currentPower = -11/*(power / 100)*/;
                        bullet.X = player.X + 10;
                        bullet.Y = player.Y + 5;

                        turretAngleRad = turretAngle * (Math.PI / 180);

                        pos = new Point(player.X, player.Y);

                        //Find the x and y velocitys using Vo*Cos(theta) and Vo*Sin(theta)
                        currentPower = power / 10;
                        bulletVelocity.X = (int)((power / 25) * Math.Cos(turretAngleRad));
                        bulletVelocity.Y = -(int)((power / 25) * Math.Sin(turretAngleRad));

                        //turn = false;

                        isGrounded = false;
                        bulletFlying = true;
                        turn = 1;
                    }
                }
            }
            else if (isBot && turn == 1)
            {
                Random random = new Random();
                power = random.Next(0, 1000);
                turretAngle = random.Next(0, 180);

                if (!bulletFlying && isGrounded)
                {
                    currentPower = -11/*(power / 100)*/;
                    bullet.X = player.X + 10;
                    bullet.Y = player.Y + 5;

                    turretAngleRad = turretAngle * (Math.PI / 180);

                    pos = new Point(player.X, player.Y);

                    //Find the x and y velocitys using Vo*Cos(theta) and Vo*Sin(theta)
                    currentPower = power / 10;
                    bulletVelocity.X = (int)((power / 25) * Math.Cos(turretAngleRad));
                    bulletVelocity.Y = -(int)((power / 25) * Math.Sin(turretAngleRad));

                    //turn = false;

                    isGrounded = false;
                    bulletFlying = true;
                    turn = 0;
                }
            }

            //Bullet code
            if (currentPower < 0)  //Bullet still has upward thrust 
            {
                currentPower++;
            }
            else
            {
                bulletFlying = false;   //Bullet now falling
            }


            //Bullet Colission
            Rectangle bulletProposedLocation = new Rectangle(bullet.X + (int)(bulletVelocity.X),
                                                    bullet.Y + (int)(bulletVelocity.Y),
                                                    bullet.Width,
                                                    bullet.Height);
            // check if move is ok
            Sides bulletCollisionSides = bulletProposedLocation.CheckCollisions(background.RigidBodyList);

            if ((bulletCollisionSides & Sides.RIGHT) == Sides.RIGHT)
            {
                if (bulletVelocity.X > 0)
                {
                    bulletVelocity.X = 0;
                }
            }
            if ((bulletCollisionSides & Sides.LEFT) == Sides.LEFT)
            {
                if (bulletVelocity.X < 0)
                {
                    bulletVelocity.X = 0;
                }
            }
            if ((bulletCollisionSides & Sides.TOP) == Sides.TOP)
            {
                bulletVelocity.Y = SPEED;
            }
            if ((bulletCollisionSides & Sides.BOTTOM) == Sides.BOTTOM && (currentPower != JUMPPOWER))
            {
                bulletVelocity.Y = 0;
                isGrounded = true;
            }

            //Tank code
            Rectangle proposedLocation = new Rectangle(player.X + (int)velocity.X,
                                                    player.Y + (int)velocity.Y,
                                                    player.Width,
                                                    player.Height);
            // check if move is ok
            Sides collisionSides = proposedLocation.CheckCollisions(background.RigidBodyList);

            if ((collisionSides & Sides.RIGHT) == Sides.RIGHT)
            {
                if (velocity.X > 0)
                {
                    velocity.X = 0;
                }
            }
            if ((collisionSides & Sides.LEFT) == Sides.LEFT)
            {
                if (velocity.X < 0)
                {
                    velocity.X = 0;
                }
            }
            if ((collisionSides & Sides.TOP) == Sides.TOP)
            {
                velocity.Y = SPEED;
            }
            if ((collisionSides & Sides.BOTTOM) == Sides.BOTTOM && (currentPower != JUMPPOWER))
            {
                velocity.Y = 0;
            }

            //absolutely ok, update location to "proposed"

            //Angle
            if (turretAngle == 0 || turretAngle == 180)
            {
                currentFrame = 1;
            }
            else if (turretAngle > 0 && turretAngle <= 8 || turretAngle < 180 && turretAngle >= 172)
            {
                currentFrame = 2;
            }
            else if (turretAngle >= 9 && turretAngle <= 14 || turretAngle <= 171 && turretAngle >= 166)
            {
                currentFrame = 3;
            }
            else if (turretAngle >= 15 && turretAngle <= 22 || turretAngle <= 165 && turretAngle >= 158)
            {
                currentFrame = 4;
            }
            else if (turretAngle >= 23 && turretAngle <= 30 || turretAngle <= 157 && turretAngle >= 150)
            {
                currentFrame = 5;
            }
            else if (turretAngle >= 31 && turretAngle <= 38 || turretAngle <= 149 && turretAngle >= 142)
            {
                currentFrame = 6;
            }
            else if (turretAngle >= 39 && turretAngle <= 41 || turretAngle <= 141 && turretAngle >= 139)
            {
                currentFrame = 7;
            }
            else if (turretAngle >= 42 && turretAngle <= 48 || turretAngle <= 138 && turretAngle >= 132)
            {
                currentFrame = 8;
            }
            else if (turretAngle >= 49 && turretAngle <= 59 || turretAngle <= 131 && turretAngle >= 121)
            {
                currentFrame = 9;
            }
            else if (turretAngle >= 60 && turretAngle <= 61 || turretAngle <= 120 && turretAngle >= 119)
            {
                currentFrame = 10;
            }
            else if (turretAngle >= 62 && turretAngle <= 75 || turretAngle <= 118 && turretAngle >= 105)
            {
                currentFrame = 11;
            }
            else if (turretAngle >= 76 && turretAngle <= 90 || turretAngle <= 104 && turretAngle >= 90)
            {
                currentFrame = 12;
            }

            //Direction
            if (turretAngle > 90)
            {
                spriteDirection = SpriteEffects.FlipHorizontally;
            }
            else if (turretAngle <= 90)
            {
                spriteDirection = SpriteEffects.None;
            }

            if (turretAngle > 180)
            {
                turretAngle = 0;
                spriteDirection = SpriteEffects.None;
            }
            else if (turretAngle < 0)
            {
                turretAngle = 180;
                spriteDirection = SpriteEffects.FlipHorizontally;
            }           

            player.X = player.X + (int)velocity.X;
            player.Y = player.Y + (int)velocity.Y;

            bullet.X = bullet.X + (int)bulletVelocity.X;
            bullet.Y = bullet.Y + (int)bulletVelocity.Y;

            base.Update(gameTime);
        }
    }
}

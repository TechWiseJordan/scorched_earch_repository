﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace JSTankGame
{
    public class ActionScene : GameScene
    {
        public HUDComponent HUDLayout { get; set; }
        
        string[] HUDItems = {"Power: ",
                             "Angle: "};
            
        

        private SpriteBatch spriteBatch;
        public ActionScene (Game game): base(game)
        {
            Game1 g = (Game1)game;
            this.spriteBatch = g.spriteBatch;

            // TODO: use this.Content to load your game content here
            Texture2D backgroundTex = g.Content.Load<Texture2D>("Backgrounds/BackgroundSunset1");
            Background b = new Background(game, spriteBatch, backgroundTex);
            Components.Add(b);
            Texture2D HUD = g.Content.Load<Texture2D>("Backgrounds/HUD");
            Background b2 = new Background(game, spriteBatch, HUD);
            Components.Add(b2);

            Texture2D playerTex = g.Content.Load<Texture2D>("Sprints/Tank1_SpriteSheet");
            Texture2D bulletTexture = g.Content.Load<Texture2D>("Sprints/1PixalBullet");
            Player p = new Player(game, spriteBatch, playerTex, bulletTexture, 200, 200, false, b);
            Components.Add(p);

            Player c = new Player(game, spriteBatch, playerTex, bulletTexture, 1000, 200, true, b);
            Components.Add(c);

            //Added HUD
            //Fonts from https://www.dafont.com/bitmap.php by Codeman38
            SpriteFont regularFont = g.Content.Load<SpriteFont>("Fonts/NewFont");
            SpriteFont highlightFont = g.Content.Load<SpriteFont>("Fonts/BoldFont");


            HUDLayout = new HUDComponent(game, spriteBatch, regularFont, HUDItems);
            this.Components.Add(HUDLayout);
            //End of HUD
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            MediaPlayer.Volume = 0.2f;
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
        }
    }
}

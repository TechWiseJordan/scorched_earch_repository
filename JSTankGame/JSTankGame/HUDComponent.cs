﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace JSTankGame
{
    public class HUDComponent : DrawableGameComponent
    {
        private SpriteBatch spriteBatch;
        private SpriteFont regularFont, hilightFont;
        private List<string> HUDItems;
        public int SelectedIndex { get; set; }
        private Vector2 position;
        private Color regularColor = Color.Black;
        private Color hilightColor = Color.Red;

        private KeyboardState oldState; // why??

        public HUDComponent(Game game, SpriteBatch spriteBatch, SpriteFont regularFont, string[] HUD) : base(game)
        {
            this.spriteBatch = spriteBatch;
            this.regularFont = regularFont;
            //menuItems = new List<string>(menus); // check if it works;
            HUDItems = HUD.ToList();  // check this later
            position = new Vector2(15, 15);

        }

        public override void Update(GameTime gameTime)
        {
            
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            Vector2 tempPos = position;
            spriteBatch.Begin();
            for (int i = 0; i < HUDItems.Count; i++)
            {
                spriteBatch.DrawString(regularFont, HUDItems[i],
                tempPos, regularColor);
                tempPos.X += 175;
            }

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
